__author__ = 'xTwentyA'

from flask_wtf import Form
from wtforms import StringField, TextAreaField
from wtforms.validators import DataRequired

class SearchForm(Form):
    query = StringField('q', validators=[DataRequired()])

class EditForm(Form):
    topic = StringField('topic', validators=[DataRequired()])
    outline = TextAreaField('outline', validators=[DataRequired()])
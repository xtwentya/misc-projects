__author__ = 'xTwentyA'

from app import db


class Note(db.Model):
    note_id = db.Column(db.Integer, primary_key=True)
    topic = db.Column(db.String)
    outline = db.Column(db.String)
    last_update = db.Column(db.DateTime)

    def __init__(self, note_id=None, topic=None, outline=None, last_update=None):
        self.note_id = note_id
        self.topic = topic
        self.outline = outline
        self.last_update = last_update
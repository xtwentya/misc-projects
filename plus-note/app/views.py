__author__ = 'xTwentyA'

from flask import render_template
from app import app, db
from app.forms import SearchForm, EditForm
from app.models import Note

import datetime

@app.route('/')
def home():
    search_form = SearchForm()
    return render_template('home.html', search_form=search_form)

@app.route('/add', methods=('GET', 'POST'))
def add():
    search_form = SearchForm()
    edit_form = EditForm()

    if edit_form.validate_on_submit():
        db.session.add(Note(topic=edit_form.topic.data, outline=edit_form.outline.data, last_update=datetime.datetime.now()))
        db.session.commit()

    return render_template('edit.html', title="Add new", search_form=search_form, edit_form=edit_form)

@app.route('/edit')
def edit():
    search_form = SearchForm()
    edit_form = EditForm()

    if edit_form.validate_on_submit():
        db.session.add(Note(topic=edit_form.topic.data, outline=edit_form.outline.data, last_update=datetime.datetime.now()))
        db.session.commit()

    return render_template('edit.html', title="Edit", search_form=search_form)

@app.route('/find', methods=('GET', 'POST'))
def find():
    search_form = SearchForm()
    return render_template('home.html', title="Search", q=" ", search_form=search_form)

@app.teardown_appcontext
def shutdown_session(exception=None):
    db.session.remove()

if __name__ == '__main__':
    app.run(debug=True)
#include <iostream>
#include <GL/freeglut.h>
#include <stdio.h>
#include <time.h>

#define CUBE_SIZE 3
#define FACE_SIZE ((CUBE_SIZE - 1) * 0.5)

enum COLOUR {
    BLACK = 0,
    RED = 1,
    BLUE = 2,
    ORANGE = 3,
    GREEN = 4,
    WHITE = 5,
    YELLOW = 6
};
GLfloat COLOURS[7][4] = {
        {0.0,   0.0,    0.0,    1.0},    //BLACK
        {0.769, 0.118,  0.227,  1.0},    //RED
        {0.0,   0.318,  0.729,  1.0},    //BLUE
        {1.0,   0.345,  0.0,    1.0},    //ORANGE
        {0.0,   0.62,   0.376,  1.0},    //GREEN
        {1.0,   1.0,    1.0,    1.0},    //WHITE
        {1.0,   0.835,  0.0,    1.0}     //YELLOW
};
GLfloat *cubeFaces[6][CUBE_SIZE][CUBE_SIZE];
GLfloat faceVertices[8][3] = {
        {-0.45, -0.45, -0.45}, {0.45, -0.45, -0.45}, {0.45, 0.45, -0.45}, {-0.45, 0.45, -0.45},
        {-0.5, -0.5, -0.5}, {0.5, -0.5, -0.5}, {0.5, 0.5, -0.5}, {-0.5, 0.5, -0.5}
};

GLfloat light_ambient[4] =  { 0.1, 0.1, 0.1, 1.0 };
GLfloat light_diffuse[4] =  { 0.2, 0.2, 0.2, 1.0 };
GLfloat light_specular[4] = { 1.0, 1.0, 1.0, 1.0 };
GLfloat light_position[4] = { 0.0, 0.0, 500.0, 0.0 };

bool isShuffling = false, isSolving = false;
float rotation[3] = {0, 0, 0};

enum TURN_DIRECTION : int {
    X_AXIS = 0,
    Y_AXIS = 1,
    Z_AXIS = 2
} turnDirection = TURN_DIRECTION::Y_AXIS;

// The order in which faces are used when turning, with the top and bottom appended on the end
int turnOrder[3][6] = {
        {0, 3, 2, 1, 5, 4},
        {0, 4, 2, 5, 3, 1},
        {4, 1, 5, 3, 2, 0}
};

// The turning 'matrix', stores which axises to use per face, per direction and whether or not to flip them.
int turnMatrix[6][3][3] = {
        {{1, 0, 1},     {0, 1, 1},      {0, 0, 0}},
        {{1, 0, 1},     {0, 0, 0},      {0, 1, -1}},
        {{1, 0, 1},     {0, -1, -1},    {0, 0, 0}},
        {{1, 0, 1},     {0, 0, 0},      {0, -1, 1}},
        {{0, 0, 0},     {0, 1, 1},      {-1, 0, -1}},
        {{0, 0, 0},     {0, 1, 1},      {1, 0, 1}}
};

int turnIndex = 0;
float turnAmount = 0.0;

struct MOVE {
    TURN_DIRECTION direction;
    int index;
    bool clockwise;
    MOVE* priorMove;
} * last_move;

int limit(int n, int modifier) {
    if(modifier < 0) n = CUBE_SIZE - n - 1;
    return n % CUBE_SIZE;
}

void rotateFace(int face, bool clockwise) {
    int x, y;
    float* newFace[CUBE_SIZE][CUBE_SIZE];
    for(y = 0; y < CUBE_SIZE; y++) {
        for(x = 0; x < CUBE_SIZE; x++) {
            if(clockwise) {
                newFace[y][x] = cubeFaces[face][x][CUBE_SIZE - y - 1];
            } else {
                newFace[x][CUBE_SIZE - y - 1] = cubeFaces[face][y][x];
            }
        }
    }

    memcpy(cubeFaces[face], newFace, CUBE_SIZE * CUBE_SIZE * sizeof(float));
}

void doTurn(bool clockwise){
    int f, i, face, *tm;
    float* temp[CUBE_SIZE], *t;

    turnAmount = clockwise ? 90 : -90;
    face = turnOrder[turnDirection][clockwise ? 3 : 0];

    // Save the last row to initialize the loop
    tm = turnMatrix[face][turnDirection];
    for (i = 0; i < CUBE_SIZE; i++) {
        temp[i] = cubeFaces[face][tm[1] == 0 ? limit(turnIndex, tm[2]) : limit(i, tm[1])][tm[0] == 0 ? limit(turnIndex, tm[2]) : limit(i, tm[0])];
    }

    for(f = 0; f < 4; f++) {
        // Get the face index
        face = turnOrder[turnDirection][clockwise ? f : 3 - f];
        // Load the turning 'matrix'
        tm = turnMatrix[face][turnDirection];

        for (i = 0; i < CUBE_SIZE; i++) {
            // Depending on whether or not it's being shifted along the x-axis or the y-axis
            // do different things to our face.
            if(tm[1] == 0) {
                t = cubeFaces[face][limit(turnIndex, tm[2])][limit(i, tm[0])];
                cubeFaces[face][limit(turnIndex, tm[2])][limit(i, tm[0])] = temp[i];
            } else {
                t = cubeFaces[face][limit(i, tm[1])][limit(turnIndex, tm[2])];
                cubeFaces[face][limit(i, tm[1])][limit(turnIndex, tm[2])] = temp[i];
            }
            temp[i] = t;
        }
    }

    // If one of the edge rows is being rotated, rotate the face on that edge
    if(turnIndex == 0) {
        rotateFace(turnOrder[turnDirection][4], !clockwise);
    } else if (turnIndex == CUBE_SIZE - 1) {
        rotateFace(turnOrder[turnDirection][5], clockwise);
    }

    // If it's being shuffled or not currently being solved, keep track of that move
    if(!isSolving || isShuffling) {
        MOVE *move = new MOVE;
        move->direction = turnDirection;
        move->index = turnIndex;
        move->clockwise = clockwise;
        move->priorMove = last_move;
        last_move = move;
    }
}

bool isInTurn(int face) {
    // Kind of messy, but efficient. Just returns true if a face is immediately affected by a turn
    return (face == turnOrder[turnDirection][0] || face == turnOrder[turnDirection][1] ||
            face == turnOrder[turnDirection][2] || face == turnOrder[turnDirection][3]);
}

bool isTileAffected(int face, int x, int y) {
    // This will return true if the tile is directly affected by a turn
    int *tm = turnMatrix[face][turnDirection];

    if (tm[2] == 0) return false;

    if(tm[1] == 0)
        return y == limit(turnIndex, tm[2]);
    else
        return x == limit(turnIndex, tm[2]);
}

void rotate(TURN_DIRECTION direction, float angle) {
    // This helps me rotate things using my own coordinate system

    switch(direction) {
        case TURN_DIRECTION::X_AXIS:
            glRotatef(angle, 0, 1, 0);
            break;
        case TURN_DIRECTION::Y_AXIS:
            glRotatef(angle, 1, 0, 0);
            break;
        case TURN_DIRECTION::Z_AXIS:
            glRotatef(angle, 0, 0, 1);
            break;
    }
}

void drawFace(float *colour, bool glow) {
    int v;
    glBegin(GL_QUADS);
        // To make it look nicer, I draw a border around each face
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, COLOURS[COLOUR::BLACK]);
        glMaterialfv(GL_FRONT, GL_EMISSION, COLOURS[COLOUR::BLACK]);
        for(v = 0; v < 4; v++) {
            glVertex3fv(faceVertices[v]);
            glVertex3fv(faceVertices[v+1]);
            glVertex3fv(faceVertices[v+4]);
            glVertex3fv(faceVertices[v+3]);
        }

        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, colour);
        if (glow) glMaterialfv(GL_FRONT, GL_EMISSION, colour);

        // Then I draw it's face
        glVertex3fv(faceVertices[4]);
        glVertex3fv(faceVertices[5]);
        glVertex3fv(faceVertices[6]);
        glVertex3fv(faceVertices[7]);

    glEnd();
}

void drawCube(void) {
    int x, y, f;
    bool turning, glow, gface;

    glPushMatrix();

    // Do the full cube rotation
    glRotatef(rotation[0], 0, 1, 0);
    glRotatef(rotation[1], 1, 0, 0);
    glRotatef(rotation[2], 0, 0, 1);

    // This animates the turns
    turnAmount *= 0.8;
    for (f = 0; f < 6; f++) {
        glPushMatrix();

        // Used to detect if a face is included in the turn
        turning = turnAmount != 0 && isInTurn(f);

        // Used to detect if a face is selected
        gface = false;
        if((turnIndex == 0 && turnOrder[turnDirection][4] == f) ||
           (turnIndex == CUBE_SIZE - 1 && turnOrder[turnDirection][5] == f)) {
            gface = true;
        }

        // If the face is selected and is turning then rotate it accordingly
        if(gface && turnAmount != 0 && !turning)    rotate(turnDirection, turnAmount);

        for (x = 0; x < CUBE_SIZE; x++)
            for (y = 0; y < CUBE_SIZE; y++) {
                glPushMatrix();
                glScalef(1.0 / CUBE_SIZE, 1.0 / CUBE_SIZE, 1.0 / CUBE_SIZE);

                // Detect whether the specific tile is affected by the turn
                glow = isTileAffected(f, x, y);

                // If the tile is affected by the turn then turn!
                if(turning && glow) rotate(turnDirection, turnAmount);

                if(f < 4)   // If the face is a side face, rotate f * 90 degrees
                    glRotatef(f * 90, 0, 1, 0);
                else        // Otherwise rotate it either on top or the bottom
                    glRotatef(f == 5 ? 90 : -90, 1, 0, 0);

                // Move into place, then draw the face in place
                glTranslatef(x - FACE_SIZE, y - FACE_SIZE, 1 + FACE_SIZE);
                drawFace(cubeFaces[f][y][x], glow | gface);

                glPopMatrix();
            }

        glPopMatrix();
    }

    glPopMatrix();
}

void initCube(void) {
    int f, x, y;
    for (f = 0; f < 6; f++)
        for (x = 0; x < CUBE_SIZE; x++)
            for (y = 0; y < CUBE_SIZE; y++)
                cubeFaces[f][x][y] = COLOURS[f + 1];

    // Do the initial shuffle
    srand(time(NULL));
    last_move = 0;
    for (f = 0; f < rand() % 100 + 20; f++) {
        turnIndex = rand() % CUBE_SIZE;
        turnDirection = (TURN_DIRECTION)(rand() % 3);
        doTurn(rand() % 2 == 1);
    }

    turnIndex = 0;
    turnAmount = 0;
    turnDirection = TURN_DIRECTION::Y_AXIS;
}

void special(int key, int x, int y) {
    if (key == GLUT_KEY_LEFT)   rotation[TURN_DIRECTION::X_AXIS] -= 15;
    if (key == GLUT_KEY_RIGHT)  rotation[TURN_DIRECTION::X_AXIS] += 15;
    if (key == GLUT_KEY_UP)     rotation[TURN_DIRECTION::Y_AXIS] -= 15;
    if (key == GLUT_KEY_DOWN)   rotation[TURN_DIRECTION::Y_AXIS] += 15;

    if (key == GLUT_KEY_SHIFT_L) turnIndex = (turnIndex + 1) % CUBE_SIZE;
    if (key == GLUT_KEY_CTRL_L) turnDirection = (TURN_DIRECTION)((turnDirection + 1) % 3);
}

void keyboard(unsigned char key, int x, int y) {
    if (key == 's') doTurn(false);
    if (key == 'd') doTurn(true);
    if (key == 't') turnAmount = 0;
    if (key == ' ') isSolving ^= true;
    if (key == 'x') isShuffling ^= true;
}

void reshape(int w, int h)
{
    glViewport(0, 0, (GLsizei) w, (GLsizei) h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (GLfloat) w/(GLfloat) h, 0.1, 50.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(1.5, 1.0, 1.5, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
}

void display(void) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    if(isShuffling && turnAmount < 0.01 && turnAmount > -0.01) {
        turnIndex = rand() % CUBE_SIZE;
        turnDirection = (TURN_DIRECTION)(rand() % 3);
        doTurn(rand() % 2 == 1);
    } else if(isSolving && turnAmount < 0.01 && turnAmount > -0.01) {
        if (last_move == 0)
            isSolving = false;
        else {
            turnIndex = last_move->index;
            turnDirection = last_move->direction;
            doTurn(!last_move->clockwise);
            MOVE * priorMove = last_move->priorMove;
            delete last_move;
            last_move = priorMove;
        }
    }
    drawCube();
    glutSwapBuffers();
    glutPostRedisplay();
}

void init(void) {
    glShadeModel(GL_SMOOTH);

    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
}

int main(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(640, 480);
    glutCreateWindow("Rubik's Cube");
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutSpecialFunc(special);
    glutKeyboardFunc(keyboard);

    initCube();
    init();
    glutMainLoop();
    return 0;             /* ANSI C requires main to return int. */
}

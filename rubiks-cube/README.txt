
	Rubik's Cube 
	- xTwentyA -

 Storing the cube in memory:
I store my cube as 6 2D arrays (each representing a face on the cube) of colours. Then whenever a column or row is
rotated, I shift colours around accordingly in each of the faces. To do this I had to create a table of what happens to
each face, depending on which axis the row is rotating about. Working this table out involved a lot of trial and error
as well as mapping out the cube on to paper several times.

 Drawing the cube:
To draw the cube I loop through each of the faces and draw the colours in the nxn sized array as if I was just drawing
them directly onto a 2D screen. Each time a tile is drawn, it will check to see if it's currently being rotated, if so
then it will rotate it accordingly. Also at this time it will check if it is currently selected, if it is then it will
glow. Finally after all tiles are drawn, the whole face will rotate to it's final position.

 Solving the cube:
As this wasn't actually a requirement, I cheated. I've just stored all the moves performed on the cube in a linked list
then whenever you decide to solve the cube, it just backtracks from the last move played.

 Controls:
    Arrow Keys  -   Rotate the cube
        S       -   Rotate the selected row anti-clockwise
        D       -   Rotate the selected row clockwise
      LSHIFT    -   Change which row is being rotated
      LCTRL     -   Change which axis the row is being rotate about
      SPACE     -   Toggle whether or not the cube is being solved
        X       -   Toggle whether or not the cube is being shuffled
        T       -   While 'T' is held, all moves are performed instantly (Useful when shuffling or solving)